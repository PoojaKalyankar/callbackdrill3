const fs = require('fs');

function employees() {
    try {
        fs.readFile('../data.json', 'utf8', function (error, data) {
            if (error) {
                console.error(error);
            } else {
                let originalData = JSON.parse(data);
                let idsArray = [2, 13, 23];
                let idsInformation = originalData.employees.filter((identity) => (idsArray.includes(identity.id)))
                fs.writeFile('../output/outputproblem1.cjs', JSON.stringify((idsInformation)), function (error) {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log("information of ids given in array")
                        let dataCompany = originalData.employees.reduce((companyInformation, currentId) => {
                            let company = currentId.company;
                            if (!companyInformation[company]) {
                                companyInformation[company] = [];
                                companyInformation[company].push(currentId);
                            } else {
                                companyInformation[company].push(currentId);
                            }
                            return companyInformation;
                        }, {});
                        console.log("group the data by company");
                        // callback(null, dataCompany);
                        fs.writeFile('../output/outputproblems2.cjs', JSON.stringify(dataCompany), function (error) {
                            if (error) {
                                console.error(error);
                            } else {
                                let powerpuffData = dataCompany['Powerpuff Brigade'];
                                fs.writeFile('../output/outputproblem3.cjs', JSON.stringify(powerpuffData), function (errorr) {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log("data of the powerpuff bridge");
                                        // console.log(originalData);
                                        let array = (Object.entries(dataCompany));
                                        let withOut2 = array.reduce((storeData, currentId) => {
                                            let name = (currentId[0]);
                                            storeData[name] = [];
                                            (currentId[1]).forEach(element => {
                                                if (element.id != 2) {
                                                    storeData[name].push(element);
                                                }
                                            });
                                            return storeData;
                                        }, {});
                                        //console.log(withOut2)
                                        fs.writeFile('../output/outputproblem4.cjs', (JSON.stringify(withOut2)), function (error) {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                console.log("id 2 is delete from company");
                                                let array = Object.entries(withOut2);
                                                let storeData = array.reduce((accumulator, currentId) => {
                                                    accumulator[currentId[0]] = []
                                                    let answer = currentId[1].sort((a, b) => {
                                                        return a.id - b.id
                                                    });
                                                    accumulator[currentId[0]].push((answer));
                                                    return accumulator;
                                                }, {});
                                                let wholeSort = Object.entries(storeData);
                                                wholeSort = (wholeSort.sort());
                                                wholeSort = Object.fromEntries(wholeSort);
                                                console.log(wholeSort)
                                                //console.log(wholeSort)
                                                fs.writeFile('../output/outputproblem5.cjs', JSON.stringify(wholeSort), function (error) {
                                                    if (error) {
                                                        console.error(error);
                                                    } else {
                                                        console.log("sorted the company by name and id");
                                                        let id93 = originalData.employees.findIndex(element => element.id == 93);
                                                        let id92 = originalData.employees.findIndex(element => element.id == 92);
                                                        let temporary = originalData['employees'][id93];
                                                        originalData['employees'][id93] = originalData['employees'][id92];
                                                        originalData['employees'][id92] = temporary;
                                                        fs.writeFile('../output/outputproblem6.cjs', JSON.stringify(originalData), function (error) {
                                                            if (error) {
                                                                console.error(error);
                                                            } else {
                                                                console.log("id's of 93 and 92 are swapped");
                                                                let today = new Date('May 18, 2024 23:15:30');
                                                                originalData.employees.forEach(element => {
                                                                    if (element.id % 2 == 0) {
                                                                        element["birthday"] = today.getDate();
                                                                    }
                                                                });
                                                                fs.writeFile('../output/outputProblem7.cjs', JSON.stringify(originalData), function (error) {
                                                                    if (error) {
                                                                        console.error(error);
                                                                    } else {
                                                                        console.log("add today date to the id is even");
                                                                    }
                                                                })
                                                            }
                                                        })

                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    } catch (error) {
        console.error(error);
    }

}


module.exports = employees;